package website.loyaltypoints.repository;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotNull;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;
import java.sql.Statement;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;
import website.loyaltypoints.service.Reservation;

public class ReservationRepositoryTests {

  private static ReservationRepository repository;

  private static final String DB_SETUP = "CREATE TABLE TBL_RESERVATIONS (id INT AUTO_INCREMENT  PRIMARY KEY, studentName VARCHAR(250) NOT NULL, studentEmail VARCHAR(250) NOT NULL, courseId VARCHAR(250) NOT NULL, reservationDate VARCHAR(250) NOT NULL);";

  private static final String DB_URL = "jdbc:h2:mem:test;DB_CLOSE_DELAY=-1";
  private static final String DB_USER = "sa";
  private static final String DB_PASS = "";

  @BeforeAll
  public static void setup() throws SQLException {

    repository = new ReservationRepositoryImpl(DB_URL, DB_USER, DB_PASS);

    try (Connection connection = DriverManager.getConnection(DB_URL,
        DB_USER, DB_PASS);
        Statement smtm = connection.createStatement();
    ) {
      smtm.executeUpdate(DB_SETUP);
      connection.commit();
    }
  }

  @Test
  public void should_save_reservation() throws SQLException {
    Reservation reservation = new Reservation("Student Name", "student.test@test.com", 3);
    Reservation savedReservation = repository.save(reservation);

    Reservation retrievedReservation = repository.find(savedReservation.id);

    assertEquals("Student Name", retrievedReservation.studentName);
    assertEquals("student.test@test.com", retrievedReservation.studentEmail);
    assertEquals(3, retrievedReservation.courseId);
    assertNotNull(retrievedReservation.reservationDate);
  }

}
