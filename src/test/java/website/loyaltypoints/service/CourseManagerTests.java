package website.loyaltypoints.service;

import org.assertj.core.api.Assertions;
import org.junit.jupiter.api.Test;
import org.mockito.Mock;
import website.loyaltypoints.repository.ReservationRepository;

public class CourseManagerTests {

  @Mock
  private ReservationRepository reservationRepository;

  @Test
  public void should_not_create_reservation() {
    CourseManager courseManager = new CourseManager(reservationRepository);
    Assertions.assertThatExceptionOfType(CursoNaoPossueVagasException.class).isThrownBy(
            () -> courseManager.createReservation(1, "Student Name", "mail.test@student.com"))
        .withMessage("Curso Não possui vagas");
  }

}
