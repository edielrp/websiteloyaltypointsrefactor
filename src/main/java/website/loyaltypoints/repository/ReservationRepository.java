package website.loyaltypoints.repository;

import java.sql.SQLException;
import website.loyaltypoints.service.Reservation;

public interface ReservationRepository {

  Reservation save(Reservation reservation) throws SQLException;

  Reservation find(String id) throws SQLException;
}
