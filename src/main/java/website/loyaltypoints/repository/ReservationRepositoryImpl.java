package website.loyaltypoints.repository;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.NoSuchElementException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import website.loyaltypoints.service.Reservation;

public class ReservationRepositoryImpl implements
    ReservationRepository {

  private static final Logger LOG = LoggerFactory.getLogger(ReservationRepositoryImpl.class);

  private static final String INSERT_RESERVATION = "INSERT INTO TBL_RESERVATIONS(studentName,studentEmail,courseId,reservationDate) VALUES (?, ?, ?, ?)";
  private static final String FIND_RESERVATION = "SELECT * FROM TBL_RESERVATIONS WHERE id = '%s'";

  private String dbUrl;
  private String user;
  private String pass;

  public ReservationRepositoryImpl(String dbUrl, String user, String pass) {
    this.dbUrl = dbUrl;
    this.user = user;
    this.pass = pass;
  }

  @Override
  public Reservation save(Reservation reservation) throws SQLException {
    try (Connection connection = DriverManager.getConnection(dbUrl, user, pass);
        PreparedStatement statement = connection.prepareStatement(
            INSERT_RESERVATION,
            Statement.RETURN_GENERATED_KEYS)) {
      LOG.debug("Inserting records into the table...");
      statement.setString(1, reservation.studentName);
      statement.setString(2, reservation.studentEmail);
      statement.setInt(3, reservation.courseId);
      statement.setString(4, String.valueOf(java.time.LocalDate.now()));
      statement.executeUpdate();

      try (ResultSet generatedKeys = statement.getGeneratedKeys()) {
        if (generatedKeys.next()) {
          String reservationId = String.valueOf(generatedKeys.getLong("id"));
          return find(reservationId);

        }

        throw new RuntimeException("Creating reservation failed, no ID obtained.");
      }
    }
  }

  @Override
  public Reservation find(String id) throws SQLException {
    try (Connection connection = DriverManager.getConnection(dbUrl, user, pass);
        Statement st = connection.createStatement();
        ResultSet rs = st.executeQuery(String.format(FIND_RESERVATION, id))) {
      if (rs.next()) {
        String studentName = rs.getString("studentName");
        String studentEmail = rs.getString("studentEmail");
        int courseId = Integer.parseInt(rs.getString("courseId"));
        String reservationDate = rs.getString("reservationDate");

        Reservation savedReservation = new Reservation(studentName, studentEmail,
            id, courseId, reservationDate);

        return savedReservation;

      }

      throw new NoSuchElementException("Reservation [ " + id + " ] not found");

    }
  }
}
