package website.loyaltypoints.service;

public class Reservation {

  public String studentName;
  public String studentEmail;
  public String id;
  public int courseId;
  public String reservationDate;

  public Reservation() {
  }

  public Reservation(String studentName, String studentEmail) {
    this.studentName = studentName;
    this.studentEmail = studentEmail;
    id = "";
  }

  public Reservation(String studentName, String studentEmail, String id, int courseId,
      String reservationDate) {
    this.studentName = studentName;
    this.studentEmail = studentEmail;
    this.id = id;
    this.courseId = courseId;
    this.reservationDate = reservationDate;
  }

  public Reservation(String studentName, String studentEmail, int courseId) {
    this.studentName = studentName;
    this.studentEmail = studentEmail;
    this.courseId = courseId;
  }

  public void setId(String id) {
    this.id = id;
  }
}
